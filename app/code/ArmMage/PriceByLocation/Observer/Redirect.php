<?php

namespace ArmMage\PriceByLocation\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Store\Model\StoreManagerInterface;

class Redirect implements ObserverInterface
{
    protected $storeManager;
    protected $curl;
    protected $session;
    protected $objectManager;

    public function __construct(
        StoreManagerInterface   $storeManager,
        Curl                    $curl,
        SessionManagerInterface $session,
        ObjectManagerInterface  $objectManager
    )
    {
        $this->storeManager = $storeManager;
        $this->curl = $curl;
        $this->session = $session;
        $this->objectManager = $objectManager;
    }

    public function execute(Observer $observer)
    {
        $isChangeStore = $this->session->getCustomRedirection();
        if (!($isChangeStore)) {
            $baseUrl = $this->storeManager->getStore()->getBaseUrl();
            $apiUrl = "http://www.geoplugin.net/json.gp";
            $this->curl->get($apiUrl);
            $response = json_decode($this->curl->getBody(), true);
            $countryCode = $response['geoplugin_countryCode'];
            if ($countryCode != null && $countryCode == 'CA') {
                //It's simple for redirect
                $redirectionUrl = $baseUrl . "stores/store/redirect/?___store=english_ca&___from_store=us_english";
                $this->session->setCustomRedirection(true);
                $observer->getControllerAction()->getResponse()->setRedirect($redirectionUrl);
            }
        }
    }
}
